import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JediInfosComponent } from './jedi-infos.component';

describe('JediInfosComponent', () => {
  let component: JediInfosComponent;
  let fixture: ComponentFixture<JediInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JediInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JediInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
