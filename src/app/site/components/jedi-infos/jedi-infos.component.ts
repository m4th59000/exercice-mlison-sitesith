import{Component, Input}from '@angular/core';

@Component({
selector: 'mat-jedi-infos',
templateUrl: './jedi-infos.component.html',
styleUrls: ['./jedi-infos.component.sass']
})
export class JediInfosComponent {
@Input() jediInfos;
}
