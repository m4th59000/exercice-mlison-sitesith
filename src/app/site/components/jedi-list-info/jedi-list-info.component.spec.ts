import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JediListInfoComponent } from './jedi-list-info.component';

describe('JediListInfoComponent', () => {
  let component: JediListInfoComponent;
  let fixture: ComponentFixture<JediListInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JediListInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JediListInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
