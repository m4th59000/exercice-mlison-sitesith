import {Component, EventEmitter, Input, Output } from '@angular/core';


@Component({
selector: 'mat-jedi-list-info',
templateUrl: './jedi-list-info.component.html',
styleUrls: ['./jedi-list-info.component.sass']
})
export class JediListInfoComponent {
@Input() jediListInfo: string;
@Output() jediClick = new EventEmitter();

  selectJedi() {
    this.jediClick.emit(this.jediListInfo);
    console.log(this.jediListInfo);
  }
}
