import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SithInfosComponent } from './sith-infos.component';

describe('SithInfosComponent', () => {
  let component: SithInfosComponent;
  let fixture: ComponentFixture<SithInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SithInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SithInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
