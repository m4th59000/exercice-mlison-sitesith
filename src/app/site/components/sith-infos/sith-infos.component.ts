import {Component, Input} from '@angular/core';

@Component({
selector: 'mat-sith-infos',
templateUrl: './sith-infos.component.html',
styleUrls: ['./sith-infos.component.sass']
})
export class SithInfosComponent {
@Input() sithInfos;
}

