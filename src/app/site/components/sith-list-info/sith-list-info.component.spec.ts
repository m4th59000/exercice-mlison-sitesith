import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SithListInfoComponent } from './sith-list-info.component';

describe('SithListInfoComponent', () => {
  let component: SithListInfoComponent;
  let fixture: ComponentFixture<SithListInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SithListInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SithListInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
