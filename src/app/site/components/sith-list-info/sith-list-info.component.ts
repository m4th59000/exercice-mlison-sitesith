import {Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
selector: 'mat-sith-list-info',
templateUrl: './sith-list-info.component.html',
styleUrls: ['./sith-list-info.component.sass']
})
export class SithListInfoComponent {
@Input() sithListInfo: string;
@Output() sithClick = new EventEmitter();

  selectSith() {
    this.sithClick.emit(this.sithListInfo);
    console.log(this.sithListInfo);
  }
}
