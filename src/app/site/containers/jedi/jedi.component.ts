import { Component } from '@angular/core';
import {Site} from "../../model/site";


@Component({
  selector: 'mat-jedi',
  templateUrl: './jedi.component.html',
  styleUrls: ['./jedi.component.sass']
})


export class JediComponent {
jediList = [
{name: 'Mace Windu', faction: 'jedi de la republique glactique', force: 4237},
{name: 'Obi-Wan Kenobi', faction: 'jedi rebelle', force: 7284},
{name: 'Luke Skywalker', faction: 'jedi gardien', force:4683},
{name: 'R2-D2', faction: 'droide rebelle', force:3255},
{name: 'Chewbacca', faction: 'contrebandier', force: 4300},
{name: 'Han Solo', faction: 'contrebandier', force: 4015},
{name: 'Qui-Gon Jinn', faction: 'jedigardien', force: 6450},
];

showDetails(jedi:Site):void{
console.log(jedi);
}
}
