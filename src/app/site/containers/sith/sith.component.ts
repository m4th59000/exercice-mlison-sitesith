import { Component} from '@angular/core';
import {Site} from "../../model/site";

@Component({
  selector: 'mat-sith',
  templateUrl: './sith.component.html',
  styleUrls: ['./sith.component.sass']
})


export class SithComponent {
sithList = [
{name: 'Dark Vitiate', faction: 'empereur Sith', force: 10765},
{name: 'Dark Malgus', faction: 'oppresseur sith', force: 9564},
{name: 'Dark Vador', faction: 'sith elu', force:8856},
{name: 'Dark Sidious', faction: 'chef sith', force:6589},
{name: 'Dark Maul', faction: 'contrebandier', force: 5300},
{name: 'Dark Revan', faction: 'elu sith', force: 13056},
{name: 'Dark Nihilus', faction: 'sith gardien', force: 7645},
];
s
showDetails(sith:Site):void{
console.log(sith);
}
}
