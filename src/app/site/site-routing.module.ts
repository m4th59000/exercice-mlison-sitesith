import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {JediComponent} from './containers/jedi/jedi.component';
import {SithComponent} from './containers/sith/sith.component';


const routes: Routes = [
  {path: '', component: JediComponent},
  {path: 'sith', component: SithComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
