import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteRoutingModule } from './site-routing.module';
import { JediComponent } from './containers/jedi/jedi.component';
import { SithComponent } from './containers/sith/sith.component';
import { JediInfosComponent } from './components/jedi-infos/jedi-infos.component';
import { JediListInfoComponent } from './components/jedi-list-info/jedi-list-info.component';
import { SithInfosComponent } from './components/sith-infos/sith-infos.component';
import { SithListInfoComponent } from './components/sith-list-info/sith-list-info.component';
import { SimpleCounterComponent } from './components/simple-counter/simple-counter.component';

@NgModule({
  declarations: [JediComponent, SithComponent, JediInfosComponent, SithInfosComponent, JediListInfoComponent, SithListInfoComponent, SimpleCounterComponent],
  imports: [
    CommonModule,
    SiteRoutingModule
  ]
})
export class SiteModule {

}
